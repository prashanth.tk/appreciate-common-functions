================
Common Functions
================


.. image:: https://img.shields.io/pypi/v/common_functions.svg
        :target: https://pypi.python.org/pypi/common_functions

.. image:: https://img.shields.io/travis/prashanth.tk/common_functions.svg
        :target: https://travis-ci.com/prashanth.tk/common_functions

.. image:: https://readthedocs.org/projects/common-functions/badge/?version=latest
        :target: https://common-functions.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Common functions contains boilerplate for code to be used across appreciate lambda, api and background services


* Free software: MIT license
* Documentation: https://common-functions.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
