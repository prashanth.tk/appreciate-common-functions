#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=7.0', ]

test_requirements = [ ]

setup(
    author="Prashanth T.K.",
    author_email='prashanth.tk@pacewisdom.com',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Common functions contains boilerplate for code to be used across appreciate lambda, api and background services",
    entry_points={
        'console_scripts': [
            'common_functions=common_functions.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='common_functions',
    name='common_functions',
    packages=find_packages(include=['common_functions', 'common_functions.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/prashanth.tk/common_functions',
    version='0.1.0',
    zip_safe=False,
)
