"""Main module."""
from decimal import Decimal


def round_decimals(value, decimals: int = 2):
    """
    Round half even
    """
    value = Decimal(value)
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    return float(value.quantize(Decimal("." + "0" * decimals)))


